var mysql      = require('mysql');
var dateFormat = require('../node_modules/dateformat/lib/dateformat');
var gpstracker = require("../lib/server");
var server = gpstracker.create().listen(8095, function(){
    console.log('listening your gps trackers on port', 8095);
});



var db_config = {
  host     : 'cloudservice-virginia.ccwb3c1eiu3d.us-east-1.rds.amazonaws.com',
  user     : 'gpstracker',
  password : 'd1$1793689',
  database : 'tracker'
};

var connection;

function handleDisconnect() {
  connection = mysql.createConnection(db_config); // Recreate the connection, since
                                                  // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();



server.trackers.on("connected", function(tracker){

    console.log("tracker connected with imei:", tracker.imei);



    tracker.on("help me", function(){
        console.log(tracker.imei + " pressed the help button!!");
    });

    tracker.on("position", function(position){

	var date = new Date();
    var now = new Date();
	var current_hour = date.getFullYear()+"-"+date.getMonth()+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
    
        console.log("tracker {" + tracker.imei +  "}: lat", position.lat, "lng", position.lng, "velocidade", position.speed ,"hora",dateFormat(now, "isoDateTime") );
   

        var speed = 0;

        if (position.speed == 'NaN') {
            speed = 0;
        } else {
            speed = position.speed;
        };



        var sql = "INSERT INTO tracker.gprmc (date, imei, latitudeDecimalDegrees, latitudeHemisphere, longitudeDecimalDegrees, longitudeHemisphere, speed, gpsSignalIndicator) VALUES ?";
        var values = [
            [dateFormat(date, "isoDateTime"), tracker.imei, position.lat, 'S', position.lng, 'W', speed, 'F']            
        ];
        connection.query(sql, [values], function(err) {
            if (err) throw err;
        });

        var queryString = 'SELECT id, imei FROM tracker.loc_atual WHERE imei = '+tracker.imei+' LIMIT 1';
        console.log(queryString);




        connection.query(queryString, function (error, results, fields) {
            if (error) {
                //
            }
            
            if (results.length  > 0) {
              //self.users = results;
              console.log('TEM LOCAL ATUAL - UPDATE');
              console.log('IMEI: ', results);

                var data = {
                    
                    date                    : dateFormat(date, "isoDateTime"),
                    latitudeDecimalDegrees  : position.lat,
                    latitudeHemisphere      : 'S',
                    longitudeDecimalDegrees : position.lng,
                    longitudeHemisphere     : 'W',
                    speed                   : position.speed
                
                };


                connection.query("UPDATE loc_atual SET ? WHERE imei = ? ",[data,tracker.imei], function(err, rows){
          
                  if (err)
                      console.log("Error Updating : %s ",err );
                
                  
                });


            }else{

              console.log('NAO TEM LOCAL ATUAL - INSERT');
              //var queryString = 'SELECT id FROM tracker.loc_atual WHERE imei = '+tracker.imei+' LIMIT 1';
           

              var sql = "INSERT INTO tracker.loc_atual (date, imei, latitudeDecimalDegrees, latitudeHemisphere, longitudeDecimalDegrees, longitudeHemisphere, speed) VALUES ?";
              var values = [
                  [dateFormat(date, "isoDateTime"), tracker.imei, position.lat, 'S', position.lng, 'W', tracker.speed]            
              ];
              connection.query(sql, [values], function(err) {
                  if (err) throw err;
              });


              connection.query(queryString, function (error, results, fields) {
                if (error) throw error;
              });

            }
        });



      });

    tracker.trackEvery(5).seconds();
});
